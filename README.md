# Sprbk Interview Test

Welcome to Springbrook interview test. The skills we will mainly observe are:

1. Analysis of the problem
2. Programming language Knowledge and experience
3. Coding best practices
    - OOP
    - SOLID principales
    - Arquitectural pattern
    - Integration Testing

### Technical Requeriments

- Visual Studio 2022
- .NET 6
- Postman

### Keywords

- CQRS
- MediaTR
- FluentValidation
- EF Core
- In Memory Database
- REST APIs
- Integration Tests

## Problem

![](./images/rent-a-car.jpg)

A car rental company hires you to develop their reservation system. The system should deal with customers’ queries providing a quote for the given dates and type of car. It should check for availability and make/cancel reservations.

### Structure

![](./images/structure.png)


### Functional Requeriments

- Display the specific vehicles available for rent by vehicle type.
- Display cost associated with a given vehicle including daily, weekend and weekly rate.
- It must also allow the user to determine the cost of a particular vehicle for a given period of time.
- The program must allow for a particular vehicle to be reserved and cancelled.

### Non Functional Requeriments

-

## Integration Tests

![](./images/postman-test.jpg)